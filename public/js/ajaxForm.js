define(require(['request']),function (Request) {
return class ajaxForm{
	constructor(confing=
		{}){
		this.form=null;
		this.req=new Request();
		this.data={};
		this.confing=confing;
		this.sendCallback=null;
		this.beforeCallback=null;
		this.errorElements=[];
		this.fileUploadedcallback=null;
	}
	OnsendCallback(callback){
		this.sendCallback=callback;
		return this;
	}
	OnbeforeCallback(callback){
	this.beforeCallback=callback;
	return this;
	}
	OnfileUploadedcallback(callback){
		this.fileUploadedcallback=callback
	return this;
	}
	initForm(){
var selector=this.confing.selector;
var form=document.querySelector(selector);
if(!form || !selector)
{
	throw	new Error('no form 	found with'+selector+'selector');

}
this.form=form;
return this;	
}
	initFileUploader()
	{

	}
	init(){
		this.initForm();
		this.initFileUploader();	
	}

}
})