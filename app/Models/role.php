<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class role extends Model
{
    protected $fillable=['role'];
    public function users(){
    	
    	return $this->belongsToMany('App\User','User_Roles','role_id','user_id');
    }
}
