<?php 
namespace App\Repo;
use Illuminate\Database\Eloquent\model;
use App\components\DataFilters\DataFilters;
class Repository{

	protected $model;
    protected $relations=[];
    protected $filter;

public function __construct(model $model,DataFilters $filter)
      {
            $this->model=$model;
            $this->filter=$filter;
      }
public function insert(array $data)
    {
if(is_assoc($data)){
	return $this->model->create($data);
    }
    else{
    	return $this->model->insert($data);	
    }
}

public function relations(array $relations  ){
 $this->relations=$relations;
 return $this;
}

public function search(array $criteria=[])
{
$this->filter->setBulider($this->model->with($this->relations));
$this->filter->setCriteria($criteria);
$this->filter->BulidQuery();
return $this->filter;
}
}