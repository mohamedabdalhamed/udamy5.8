<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Brand;
use App\Repo\BrandRepo;
class BrandController extends Controller
{
        public function __construct()
    {
        $this->middleware('auth')->only(['manage']);
    }

public function manage(Request $req,BrandRepo $brand)
{

$this->Authorize('manage',Brand::class);
foreach (range(1,100) as $i ) 
{
$brand->insert(['name'=>'brand'.$i]);

}
$df=$brand->search();
$data=$df->getData();
$view_data['data']=$data;
if($req->ajax()){
	$view=view('brand.manage-list')->with($data) ->render();

}
return view('brand.manage',$view_data);
}
}
