<!DOCTYPE html>
<html >
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Laravel') }}</title>
    <link rel="stylesheet" type="text/css" href="{{ asset('css/grid.css') }}">

<link rel="stylesheet" type="text/css" href="{{ asset('css/public.css') }}">

<link rel="stylesheet" type="text/css" href="{{ asset('css/header.css') }}"  >
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="{{ asset('css/footer.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/account.css') }}">
  <script type="text/javascript" src="{{asset('js/helpers.js')}}"></script>
<script type="text/javascript" src="{{asset('js/require.js')}}" data-main="{{asset('js/main.js')}}"></script>

</head>
<body>
    @include('partials.header')
    <div class="main account-main cf">
<div class="left account-sidebar">
    <div class="row">
        <div class="col col-12">
                            @include('partials.account-sidebar')

        </div>
    </div>

</div>

<div class="left account-main-content">
    <div class="row">
        <div class="col col-12">
                    @yield('main')

      
</div>
    </div>
</div>
        @include('partials.footer')

<script
  src="https://code.jquery.com/jquery-3.5.1.js"
  integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc="
  crossorigin="anonymous"></script>
                      @yield('js')

    <script type="text/javascript" >

        adjustsildebarHeight();
        function adjustsildebarHeight() {
            var main_account_content=$('.account-main-content').height();
            console.log(main_account_content);
            $('.account-sidebar').css({height:main_account_content+'px'});
        }
    </script>

</body>
</html>