@if($data->count())
<div style="margin:10px 0;">
	{{$data->links()}}
</div>
<div class="table-wrappe">
	<table >
		<thead>
			<tr>
					@if(input('wcrud') || input('wselect'))
				<td></td>
			
@endif
				<th>#</th>
				<th>Brand Name</th>

				@if(input('wcrud'))
				<th><div class="dropdown a-dropdown">
					<a href="" class="btn btn45 dropdown-toggle"><span class="toggle-text">Bluck action <i class="fa fa-caret-down"></i></span></a>
					<ul class="dropdown-menu">
						<li><a href="" class="menu-item" data-text="Delete">Delete all</a></li>
						<li><a href="" class="menu-item" data-text="Clear">Clear all</a></li>
					</ul>
				</div></th>
			
@endif
		
			</tr>
		</thead>
		 <tbody>
		 	@php
		 	$index=($data->currentPage()-1)*$data->perPage()+1;
		 	@endphp
		 	@foreach($data as $d)
		 	<tr>

					@if(input('wcrud') || input('wselect'))
				<td style="position: relative;"><span style="float: left;
				width: 15px;height: 15px;background: \#ededed;border: 1px solid #ccc;cursor: pointer;
				" class="select-box"></span></td>
			
@endif
		 		<td>{{$index}}</td>
		 		<td>{{$d->name}}</td>
		 			@if(input('wcrud'))
				<th><form action="{{route('brand_delete',['id'=>$d->id])}}" method="POST">
					@csrf
					@method('DELETE')
					<input type="submit" class="btn" value="DELETE"name="">
				</form></th>
@endif
		
		 	</tr>
		 	@php
		 	$index++;
		 	@endphp
		 	@endforeach
		 </tbody>
	</table>
</div>
<div style="margin:10px 0;">
	{{$data->links()}}
</div>
@else

<p>no data found</p>
@endif
<script type="text/javascript">
	dropdowns();
</script>
@section('js')
<script type="text/javascript">
	require(['request'],function (x) {
		console.log(x);	})
</script>
@endsection